<?php

namespace App\backend\Controller\User;

use App\User\Application\Command\CreateUserCommand;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class CreateUserController
{
    private ValidatorInterface $validator;
    private MessageBusInterface $commandBus;

    public function __construct(ValidatorInterface $validator, MessageBusInterface $commandBus)
    {
        $this->validator = $validator;
        $this->commandBus = $commandBus;
    }

    public function __invoke(string $id, Request $request): Response
    {
        $data = json_decode($request->getContent(), true);
        if($this->validateRequest($data)->count() > 0){
            throw new BadRequestHttpException();
        }

        $this->commandBus->dispatch(new CreateUserCommand($id, $data['name'], $data['phone']));

        return new Response(null, Response::HTTP_CREATED);
    }

    protected function validateRequest(array $data): \Countable
    {
        $constraint = new Assert\Collection([
            'name' => new Assert\Length(['min' => 3, 'max' => 100]),
            'phone' => new Assert\Regex("/^\d{9}$/")
        ]);

        return $this->validator->validate($data, $constraint);
    }
}
