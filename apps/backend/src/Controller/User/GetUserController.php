<?php

namespace App\backend\Controller\User;

use App\User\Application\Query\GetUserQuery;
use App\User\Domain\User;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\Exception\HandlerFailedException;
use Symfony\Component\Messenger\HandleTrait;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Serializer\SerializerInterface;

class GetUserController
{
    use HandleTrait;
    private SerializerInterface $serializer;

    public function __construct(MessageBusInterface $queryBus, SerializerInterface $serializer)
    {
        $this->messageBus = $queryBus;
        $this->serializer = $serializer;
    }

    public function __invoke(string $id): Response
    {
        try {
            /** @var User $user */
            $user = $this->handle(new GetUserQuery($id));
        }catch (HandlerFailedException $exception){
            return new Response(null, Response::HTTP_NOT_FOUND); //TODO: Refactor
        }

        return new Response(
            $this->serializer->serialize($user, 'json'),
            Response::HTTP_OK,
            ['Content-type' => 'application/json']
        );
    }
}
