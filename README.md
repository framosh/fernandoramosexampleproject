Slim me dió algo de guerra al intentar instalar Doctrine y Symfony Messenger, por lo que decidí instalar un Symfony de cero y partir de ahí.

He intentando hacer un enfoque DDD con CQRS apoyado en el uso de Symfony Messenger con varios buses configurados (Ver _config/packages/messenger.yaml_ y _config/services.yaml_)

- Commands (escrituras). Se manejan implementando: _App\Shared\Domain\Command\CommandHandler_
- Query (lecturas). Se manejan implementando: _App\Shared\Domain\Query\QueryHandler_
- EventSubscriber (eventos de dominio). Se publican eventos de dominios a los que uno se puede suscribir implementando App\Shared\Domain\Event\EventSubscriber

Los Commands y Queries de momento están configurados de forma síncrona (en el caso de Commands se podrían hacer asíncronos sin problemas).

En cambio los eventos de dominio se consumen de forma asíncrona con el comando:

```
bin/console messenger:consume async -vv
```
Hay un ejemplo de como se consumen estos eventos de dominio en `src/User/Application/EventSubscriber/UserCreatedSendSmsEventSubscriber.php`

Como hablamos, creo que ver esta prueba juntos será bastante mejor para explicar paso por paso algunas cosas.

He creado algunos tests unitarios y los tests de Behat pasan correctamente.

Os dejo también un listado de cosas que haría a continuación:

1. Al crear un usuario, sería necesario validar que el ID del usuario no esté en uso (y el teléfono seguramente igual).
2. Dentro del usuario, el teléfono debería ser un ValueObject independiente. Igual que el UserId, crearía clases independientes.
3. Las validaciones que hay en la creación de usuario quizás las metería en otra clase, no me convence mucho que estén en el controlador.
4. Cuando ocurre un error en la validación de un usuario nuevo, sería interesante devolver en el JSON que error ha ocurrido.
5. No me gusta el try/catch en el GetUserController. Esto está así porque cuando se emite una Query y en el QueryHandler se lanza una excepción, lo que llega al controlador de vuelta es una HandlerFailedException, no es la excepción original que lanzaste en el manejador. La HandlerFailedExcepcion lleva dentro la excepción original, por lo que quizás haría un dispatcher propio que maneje esto. Ver https://stackoverflow.com/a/59534691
6. El ID de usuario lo generaría en la propia aplicación (con https://symfony.com/doc/current/components/uid.html quizás), en lugar de que me venga el ID del nuevo usuario en el PUT.
7. Faltan tests unitarios y de Behat, he hecho algunos pero se podrían hacer más y quizás mejorar lo que hay.

-------------------------------------------
 El equipo de coches.com tiene un problema con el microservico de usuarios y necesita tu ayuda para solucionarlo.    
    
El número de usuarios crece muy rápido y el equipo de producto no hace nada más que pedir nuevas features.   
El problema es que el código es muy legacy y no se presta ni a escalar ni es capaz de aguantar el tráfico.    
    
Tú prueba consiste en hacer que este microservicio sea más escalable y más mantenible.    
    
**¿Te apetece intentarlo?**  
  
**Índice**  
1. [Requisitos de la prueba](#requisitos-de-la-prueba)
2. [¿Qué necesitas?](#qué-necesitas)
3. [Instalación](#instalación)
4. [Cómo añadir dependencias nuevas](#cómo-añadir-dependencias-nuevas)
5. [Documentación](#documentación)
6. [Testing](#testing)
7. [Cosas que tendremos en cuenta en la evaluación](#cosas-que-tendremos-en-cuenta-en-la-evaluación)
8. [Cosas que NO tendremos en cuenta en la evaluación](#cosas-que-no-tendremos-en-cuenta-en-la-evaluación)
9. [¿Cómo finalizar la prueba?](#cómo-finalizar-la-prueba)
10. [¿Puedo hacer la prueba sin docker?](#puedo-hacer-la-prueba-sin-docker)
11. [Tengo problemas para realizar la prueba](#tengo-problemas-para-realizar-la-prueba)


<h2 id="requisitos-de-la-prueba">Requisitos de la prueba</h2>

Sólo tienes una restricción **respeta la interfaz de la API** ya que tenemos muchos clientes usándola.  
Puedes usar todas las librerías o frameworks que quieras o que te sientas cómodo.  
    
Puedes tirar por dos caminos 100% válidos:    
1. Tirarlo todo y hacerlo como más te guste    
2. Refactorizarlo y dejarlo a tu gusto    
    
La prueba tiene un tiempo máximo de **2 horas.** Si no terminas no pasa nada, cuentanos por donde hubieses seguido y     
también es válido.  
No vamos a evaluar cuanto has hecho si no como lo has hecho, asi que 2 horas máximo.  

<h2 id="que-necesitas">¿Qué necesitas?</h2>

1. Tener git
2. Tener docker
    
Si decides hacer la prueba sin docker [aquí](#puedo-hacer-la-prueba-sin-docker) tienes como hacerlo.  
    
<h2 id="instalacion">Instalación</h2>

1. Clónate el repositorio    
2. Ejecuta `make build` Esta ejecución tarda un poco, mientras... te aconsejamos que termines de leer toda la prueba.    
    
Después de ejecutar el último comando se te han levantado 2 contenedores:    
1. Un apache con php 7.4 que está corriendo en el _puerto 8000_ y el xdebug en el _puerto 9013_ (php-technical-test)    
2. Un mariadb 10.5 que está corriendo en el _puerto 3366 usuario: root password: admin_ (mariadb-technical-test)     
    
<h2 id="como-anadir-dependencias-nuevas">Cómo añadir dependencias nuevas</h2>

Puedes añadir dependencias de dos formas:
1. Manualmente modificando el composer.json y ejecutando `make composer-update`
2. Ejecutando `make composer CMD="require <dependencia> --ignore-platform-reqs"`    

<h2 id="documentacion">Documentación</h2>

En el directorio doc tienes un fichero swagger.yaml con las especificación de la API https://editor.swagger.io/    
    
Tambien tienes un api.http con la llamada a los dos endpoints si trabajas con un editor de jetbrains.    
    
Si trabajas con otro editor tienes un .sh con los curl.    
    
Con todo y con eso aquí tienes un ejemplo de los 2 endpoints de la prueba:    
```
curl -d '{"name":"Pedro", "phone":"607112235"}' \  
 -H "Content-Type: application/json" \ -X PUT http://localhost:8000/users/9cec71c0-3906-45cc-b8a0-1bd50621c4d5 
``` 
``` 
curl http://localhost:8000/users/9cec71c0-3906-45cc-b8a0-1bd50621c4d5  
  # return {"id":"9cec71c0-3906-45cc-b8a0-1bd50621c4d5","name":"Pedro","phone":"607112235"} 
```    

<h2 id="testing">Testing</h2>

Para facilitar el testing hemos añadido unos test de behat que podrás usar siempre que necesites para verificar si tu   
la API esta funcionando correctamente. Para ejecutarlos:
``` 
make behat 
```

También hemos dejado los tests de php-unit ya configurados y con un test de prueba en `test/src/Unit`.
   
Para ejecutarlos simplemente debes ejecutar:
```
make unit
```

<h2 id="cosas-que-tendremos-en-cuenta">Cosas que tendremos en cuenta en la evaluación</h2>

Cuando evaluemos la prueba tendremos en cuenta:
1. La limpieza del código
2. Si has hecho el desarrollo con TDD
3. Si has usado alguna arquitectura como DDD o hexagonal
4. La extructura de directorios que has usado.
5. Si no te ha dado tiempo de terminar la prueba, como nos trasmites por escrito por donde hubieses continuado.

<h2 id="cosas-que-no-tendremos-en-cuenta">Cosas que NO tendremos en cuenta en la evaluación</h2>

NO se va a tener en cuenta si el código funciona o no. Creemos que puede no darte tiempo a terminar y no pasaría nada, lo entendemos.
Eso sí, cuentanos como hubieses seguido.

<h2 id="como-finalizar-la-prueba">¿Cómo finalizar la prueba?</h2>

Habrás recibido un email donde te invitamos a participar en una merge request y una rama con tu nombre.
En esa rama tienes todo lo necesario para hacer la prueba.

Una vez que termines pushea todos los commit en esa rama y avisanos via email.

Si quieres dejar un comentario, puedes hacerlo sin problemas en la merge request.
    
<h2 id="puedo-hacer-la-prueba-sin-docker">¿Puedo hacer la prueba sin docker?</h2>

Por supuesto que sí.  
  
Necesitas un servidor web (apache + php o nginx + php o xampp), base de datos (mysql o mariadb) y composer.  
1. Configura tu servidor para que el documentRoot sea `apps/backend/public`  
2. Ejecuta en tu base de datos el .sql que se encuentra en: `docker/mariadb` 3. Sustituye en `src/User/User.php` todas las conexiones a la base de datos  
4. Sustituye en `behat.yml` los parámetros de conexión a la base de datos y la url  
5. Ejecuta `composer install`  
6. Para ejecutar los tests de behat `vendor/bin/behat`  
7. Para ejecutar los tests de phpUnit `vendor/bin/phpunit --configuration phpunit.xml`  

<h2 id="tengo-problemas-para-realizar-la-prueba">Tengo problemas para realizar la prueba</h2>

Si tienes problemas para realizar la prueba dínoslo porque como te hemos dicho el código es muy legacy.
