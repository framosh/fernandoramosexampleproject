<?php

namespace App\Shared\Domain\Model;

use App\Shared\Domain\Event\DomainEvent;

abstract class Aggregate
{
    /** @var DomainEvent[] */
    private array $events = [];

    public function add(DomainEvent $domainEvent): void
    {
        $this->events[] = $domainEvent;
    }

    public function getDomainEventsAndClear(): array
    {
        $events = $this->events;

        $this->events = [];
        return $events;
    }
}
