<?php

namespace App\Shared\Domain;

interface DomainException
{
    public function errorMessage(): string;
}
