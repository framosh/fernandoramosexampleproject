<?php

namespace App\User\Domain;

interface UserRepository
{
    public function find(string $id): ?User;
}
