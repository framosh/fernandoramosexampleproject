<?php

namespace App\User\Domain;

use App\Shared\Domain\DomainException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class UserNotFoundException extends NotFoundHttpException implements DomainException
{
    private string $userId;

    public function __construct(string $userId)
    {
        $this->userId = $userId;
        parent::__construct($this->errorMessage());
    }

    public function errorMessage(): string
    {
        return sprintf('User with id %s not found', $this->userId);
    }
}
