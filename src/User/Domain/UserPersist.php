<?php

namespace App\User\Domain;

interface UserPersist
{
    public function save(User $user): void;
}
