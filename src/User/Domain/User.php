<?php

namespace App\User\Domain;

use App\Shared\Domain\Model\Aggregate;

class User extends Aggregate
{
    private string $id;
    private string $name;
    private string $phone;

    public function __construct(string $id, string $name, string $phone)
    {
        $this->id = $id;
        $this->name = $name;
        $this->phone = $phone;
    }

    public static function build(string $id, string $name, string $phone)
    {
        $user = new self($id, $name, $phone);
        $user->add(new UserCreatedEvent($user));

        return $user;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): void
    {
        $this->id = $id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): void
    {
        $this->phone = $phone;
    }
}
