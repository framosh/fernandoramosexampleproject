<?php

namespace App\User\Domain;

use App\Shared\Domain\Event\DomainEvent;

class UserCreatedEvent implements DomainEvent
{
    private User $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function getUser(): User
    {
        return $this->user;
    }
}
