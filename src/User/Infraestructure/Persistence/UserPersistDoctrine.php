<?php

namespace App\User\Infraestructure\Persistence;

use App\User\Domain\User;
use App\User\Domain\UserPersist;
use Doctrine\ORM\EntityManagerInterface;

class UserPersistDoctrine implements UserPersist
{
    private EntityManagerInterface  $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function save(User $user): void
    {
        $this->entityManager->persist($user);
        $this->entityManager->flush();
    }
}
