<?php

namespace App\User\Infraestructure\Persistence;

use App\User\Domain\User;
use App\User\Domain\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectRepository;

class UserRepositoryDoctrine implements UserRepository
{
    private ObjectRepository $doctrineRepository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->doctrineRepository = $entityManager->getRepository(User::class);
    }

    public function find(string $id): ?User
    {
        return $this->doctrineRepository->find($id);
    }
}
