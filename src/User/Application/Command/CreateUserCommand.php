<?php

namespace App\User\Application\Command;

use App\Shared\Domain\Command\Command;

class CreateUserCommand implements Command
{
    protected string $id;
    protected string $name;
    protected string $phone;

    public function __construct(string $id, string $name, string $text)
    {
        $this->id = $id;
        $this->name = $name;
        $this->phone = $text;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }
}
