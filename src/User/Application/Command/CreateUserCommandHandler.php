<?php

namespace App\User\Application\Command;

use App\Shared\Domain\Command\CommandHandler;
use App\User\Domain\User;
use App\User\Domain\UserPersist;
use Symfony\Component\Messenger\MessageBusInterface;

class CreateUserCommandHandler implements CommandHandler
{
    private UserPersist $userPersist;
    private MessageBusInterface $eventBus;

    public function __construct(UserPersist $userPersister, MessageBusInterface $eventBus)
    {
        $this->userPersist = $userPersister;
        $this->eventBus = $eventBus;
    }

    public function __invoke(CreateUserCommand $createUserCommand): void
    {
        $user = User::build($createUserCommand->getId(), $createUserCommand->getName(), $createUserCommand->getPhone());

        $this->userPersist->save($user);

        $this->eventBus->dispatch(...$user->getDomainEventsAndClear());
    }
}
