<?php

namespace App\User\Application\Query;

use App\Shared\Domain\Query\QueryHandler;
use App\User\Domain\User;
use App\User\Domain\UserNotFoundException;
use App\User\Domain\UserRepository;

class GetUserQueryHandler implements QueryHandler
{
    private UserRepository $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function __invoke(GetUserQuery $getUserQuery): User
    {
        $user = $this->userRepository->find($getUserQuery->getId());

        if (!$user) {
            throw new UserNotFoundException($getUserQuery->getId());
        }

        return $user;
    }
}
