<?php

namespace App\User\Application\EventSubscriber;

use App\Shared\Domain\Event\EventSubscriber;
use App\User\Domain\UserCreatedEvent;

class UserCreatedSendSmsEventSubscriber implements EventSubscriber
{
    public function __invoke(UserCreatedEvent $userCreatedEvent): void
    {
        echo 'An SMS will be sent asynchronously to the user which will take 10 seconds ...'.PHP_EOL;
        sleep(10);
        echo 'SMS sent!!'.PHP_EOL;
    }
}
