<?php

namespace App\Tests\src\Unit\User\Application;

use App\User\Application\Command\CreateUserCommand;
use App\User\Application\Command\CreateUserCommandHandler;
use App\User\Domain\User;
use App\User\Domain\UserCreatedEvent;
use App\User\Domain\UserPersist;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\MessageBusInterface;

class CreateUserCommandHandlerTest extends TestCase
{
    public function testInvoke(): void
    {
        $userPersister = $this->createMock(UserPersist::class);
        $userPersister
            ->expects($this->once())
            ->method('save')
            ->with($this->isInstanceOf(User::class));

        $messageBus = $this->createMock(MessageBusInterface::class);
        $messageBus
            ->expects($this->once())
            ->method('dispatch')
            ->with($this->isInstanceOf(UserCreatedEvent::class))
            ->willReturn(new Envelope($this->createMock(UserCreatedEvent::class)));
        ;

        $createUserCommand = new CreateUserCommand('fakeId', 'name', '615273628');
        $handler = new CreateUserCommandHandler($userPersister, $messageBus);
        $handler($createUserCommand);
    }
}
