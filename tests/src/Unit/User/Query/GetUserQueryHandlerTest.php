<?php

namespace App\Tests\src\Unit\User\Query;

use App\User\Application\Query\GetUserQuery;
use App\User\Application\Query\GetUserQueryHandler;
use App\User\Domain\User;
use App\User\Domain\UserNotFoundException;
use App\User\Domain\UserRepository;
use PHPUnit\Framework\TestCase;

class GetUserQueryHandlerTest extends TestCase
{
    public function testInvokeUserExists(): void
    {
        $user = new User('fakeId', 'fakeName', '666555444');

        $userRepository = $this->createMock(UserRepository::class);
        $userRepository
            ->expects($this->once())
            ->method('find')
            ->willReturn($user);

        $handler = new GetUserQueryHandler($userRepository);
        $userFind = $handler(new GetUserQuery('fakeId'));
        $this->assertEquals($user, $userFind);
    }

    public function testInvokeUserNotExistsThrowUserNotFoundException(): void
    {
        $userRepository = $this->createMock(UserRepository::class);
        $userRepository
            ->expects($this->once())
            ->method('find')
            ->willReturn(null);

        $this->expectException(UserNotFoundException::class);

        $handler = new GetUserQueryHandler($userRepository);
        $handler(new GetUserQuery('fakeId'));
    }
}
