<?php

namespace App\Tests\src\Unit\Controller\User;

use App\backend\Controller\User\CreateUserController;
use App\User\Application\Command\CreateUserCommand;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\HandledStamp;
use Symfony\Component\Messenger\Stamp\StampInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class CreateUserControllerTest extends TestCase
{
    public function testInvoke(): void
    {
        $errors = $this->createMock(\Countable::class);
        $errors
            ->expects($this->once())
            ->method('count')
            ->willReturn(0);

        $validator = $this->createMock(ValidatorInterface::class);
        $validator
            ->expects($this->once())
            ->method('validate')
            ->willReturn($errors);

        $messageBusInterface = $this->createMock(MessageBusInterface::class);
        $messageBusInterface
            ->expects($this->once())
            ->method('dispatch')
            ->with($this->isInstanceOf(CreateUserCommand::class))
            ->willReturn(new Envelope($this->createMock(CreateUserCommand::class)));

        //La linea de abajo con PHP 8 es mucho más elegante: https://stitcher.io/blog/php-8-named-arguments
        $request = new Request([], [], [], [], [], [], json_encode(['name' => 'test', 'phone' => '666555444']));

        $createUser = new CreateUserController($validator, $messageBusInterface);
        $response = $createUser('fakeId', $request);

        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());
    }

    //TODO: hacer otro test para comprobar la excepción si hay errores
}
