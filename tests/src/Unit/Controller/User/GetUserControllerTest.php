<?php

namespace App\Tests\src\Unit\Controller\User;

use App\backend\Controller\User\GetUserController;
use App\User\Application\Command\CreateUserCommand;
use App\User\Application\Query\GetUserQuery;
use App\User\Domain\User;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\HandledStamp;
use Symfony\Component\Messenger\Stamp\StampInterface;
use Symfony\Component\Serializer\SerializerInterface;

class GetUserControllerTest extends TestCase
{
    public function testInvoke(): void
    {
        $user = new User('fakeId', 'fakeName', '666777888');

        //Thank you https://git.dinecat.com/dinecat/cqrs/blob/9760c63d5f5be0a43df413171873a630ea69c453/tests/Unit/CommandBus/CommandBusTest.php
        $envelope = new Envelope(
            $this->createMock(GetUserQuery::class),
            [new HandledStamp($user, '')]
        );

        $messageBus = $this->createMock(MessageBusInterface::class);
        $messageBus
            ->expects($this->once())
            ->method('dispatch')
            ->with($this->isInstanceOf(GetUserQuery::class))
            ->willReturn($envelope);


        $serializer = $this->createMock(SerializerInterface::class);
        $serializer
            ->expects($this->once())
            ->method('serialize')
            ->with($user, 'json');

        $getUser = new GetUserController($messageBus, $serializer);
        $response = $getUser('fakeId');
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }
}
